Placeholder alternativo para navegadores antigos
====

**Composta por 2 métodos:**<br>
- onde é verificado o suporte a atributos do html5
- e caso exista um atributo placeholder no elemento informado e o navegador não seja capaz de renderizar, o valor do mesmo é aplicado no seu atributo **value** .

<br>

Placeholder alternative for older browsers
====

**Consisting of 2 methods:**<br>
- where is verified support for html5 attributes
- and if there is an attribute in the placeholder element informed and the browser is not able to render the value of the same attribute is applied to its **value** .